package pl.sda.tdd.start;


public class MyNumber {

    /*public boolean isPrimeNumber(int number) {
        if (number <= 1 || number % 2 == 0 || number % 3 == 0)
            return false;
        return true;
    }*/

    public boolean isPrimeNumber(int number) {
        int counter = 0;
        for(int i = 1; i < 10000; i+=2) {
            if(number%i==0) {
                counter++;
            }
        }
        return counter == 2;
    }
}
