package pl.sda.tdd.start;

public class QuadraticEquation {
    public QuadraticEquation(double a, double b, double c) {
        this.a = a;
        this.b = b;
        this.c = c;
    }

    double delta;
    double a;
    double b;
    double c;

    private  double calculateDelta() {
        if (a != 0)
        delta = b*b - (4*a*c);
        return delta;

    }

    public double[] calculateRoots() {
        double[] rootsArray;
        if(calculateDelta()>0) {
            rootsArray = new double[2];
            rootsArray[0] = (-b - Math.sqrt(calculateDelta())) / (2*a);
            rootsArray[1] = (-b + Math.sqrt(calculateDelta())) / (2*a);
        } else if(calculateDelta() == 0) {
            rootsArray = new double[1];
            rootsArray[0] = -b / 2*a;
        } else {
            rootsArray = new double[0];
        }
        return rootsArray;
    }

}
