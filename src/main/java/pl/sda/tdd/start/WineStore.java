package pl.sda.tdd.start;

public class WineStore {

    private int numberOfCoupons = 0;

    public void makePurchase(int cost) {
        int realCost;
        if(!isRegularCustomer()) {
            realCost = calculateDiscount(cost);
        } else {
            realCost = calculateRegularCustomerDiscount(cost);
        }
        if (realCost >= 40) {
            numberOfCoupons += 1;
        }
        if (realCost >= 80) {
            numberOfCoupons += 1;
        }
        if (realCost >= 120) {
            numberOfCoupons += 1;
        }
    }

    private int calculateRegularCustomerDiscount(int cost) {
        if (cost > 100)
            return (int) (cost * 0.87);
        else if (cost > 50) {
            return (int) (cost * 0.92);
        }
        return cost;
    }

    private int calculateDiscount(int cost) {
        if (cost > 100)
            return (int) (cost * 0.9);
        else if (cost > 50) {
            return (int) (cost * 0.95);
        }
        return cost;
    }

    private boolean isRegularCustomer() {
        return numberOfCoupons >= 10;
    }

    public int getReceivedCouponNumber() {
        return numberOfCoupons;
    }
}
