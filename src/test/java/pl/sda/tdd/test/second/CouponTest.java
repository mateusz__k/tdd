package pl.sda.tdd.test.second;


import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import pl.sda.tdd.start.WineStore;

import java.util.Arrays;
import java.util.Collection;

@RunWith(Parameterized.class)

public class CouponTest {

    @Parameterized.Parameter(value = 0)
    public int purchaseCost;

    @Parameterized.Parameter(value = 1)
    public int expectedCouponNumber;

    private WineStore wineStore;

    @Before
    public void setup() {
        wineStore = new WineStore();
    }

    @Parameterized.Parameters
    public static Collection<Object[]> dataProvider() {
        return Arrays.asList(new Object[][]{
                {39, 0},
                {40, 1},
                {84, 1},
                {85, 2},
                {133, 2},
                {134, 3},
                {250, 3},
        });
    }

    @Test
    public void numberOfCouponsTest() {
        wineStore.makePurchase(purchaseCost);
        Assert.assertEquals(expectedCouponNumber, wineStore.getReceivedCouponNumber());
    }
}
