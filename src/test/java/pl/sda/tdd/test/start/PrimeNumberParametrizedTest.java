package pl.sda.tdd.test.start;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import pl.sda.tdd.start.MyNumber;

import java.util.Arrays;
import java.util.Collection;

@RunWith(Parameterized.class)
public class PrimeNumberParametrizedTest {

    private int number;
    private boolean isPrimeNumber;

    public PrimeNumberParametrizedTest(int number, boolean isPrimeNumber) {
        this.number = number;
        this.isPrimeNumber = isPrimeNumber;
    }

    @Test
    public void shouldBePrimeNumber() {
        MyNumber myNumber = new MyNumber();
        Assert.assertEquals(isPrimeNumber, myNumber.isPrimeNumber(number));
    }


    @Parameterized.Parameters (name = "{index} {0} should be prime number? {1}")
    public static Collection<Object[]> dataProvider(){
        return Arrays.asList(new Object[][]{
                {1, false},
                {0, false},
                {5, false},
                {11, true},
                {12, false},
                {99, false},
                {100, false}
        });
    }


}
