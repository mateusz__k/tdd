package pl.sda.tdd.test.start;
import org.junit.BeforeClass;
import org.junit.Test;
import pl.sda.tdd.start.MyNumber;

import static junit.framework.TestCase.assertTrue;
import static org.junit.Assert.assertFalse;

public class PrimeNumberTest {
    static MyNumber number;

    @BeforeClass
    public static void setup() {
        number = new MyNumber();
    }

    @Test
    public void oneIsNotAPrimeNumber() {
        assertFalse(number.isPrimeNumber(1));
    }

    @Test
    public void zeroIsNotAPrimeNumber() {
        assertFalse(number.isPrimeNumber(0));
    }

    @Test
    public void fiveIsAPrimeNumber() {
        assertTrue(number.isPrimeNumber(5));
    }

    @Test
    public void nineIsNotAPrimeNumber() {
        assertFalse(number.isPrimeNumber(9));
    }

    @Test
    public void elevenIsAPrimeNumber () {
        assertTrue(number.isPrimeNumber(11));
    }

    @Test
    public void hundredIsNotAPrimeNumber() {
        assertFalse(number.isPrimeNumber(100));
    }

   //  \/\/\/ TESTY PARAMETRYZOWANE \/\/\/

}
