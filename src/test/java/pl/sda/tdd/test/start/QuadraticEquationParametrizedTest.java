package pl.sda.tdd.test.start;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import pl.sda.tdd.start.QuadraticEquation;

import java.util.Arrays;
import java.util.Collection;

import static org.junit.Assert.assertArrayEquals;

@RunWith(Parameterized.class)
public class QuadraticEquationParametrizedTest {

    @Parameterized.Parameter(value = 0)
    public int a;

    @Parameterized.Parameter(value = 1)
    public int b;

    @Parameterized.Parameter(value = 2)
    public int c;

    @Parameterized.Parameter(value = 3)
    public double[] expectedResult;

    @Test
    public void test(){
        QuadraticEquation qe = new QuadraticEquation(a,b,c);
        assertArrayEquals(expectedResult, qe.calculateRoots(), 0.01d);
    }



    private static double[] stringToArray(String str){
        if(str.equals(""))
            return new double[0];
        String[] splitted = str.split(",");
        double[] intArray = new double[splitted.length];
        for(int i=0; i<splitted.length; i++)
            intArray[i]=Integer.parseInt(splitted[i]);
        return intArray;
    }

    @Parameterized.Parameters/*(name = "{index}: {0}x^2 + {1}x + {2} -> [{3}]")*/
    public static Collection<Object[]> dataProvider(){
        return Arrays.asList(new Object[][]{
                {1,0,-1, stringToArray("-1,1")},
                {1,2,1, stringToArray("-1")},
                {1,1,1, ""}
        });
    }
}